<?php

namespace Drupal\search_api_reindex_related;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Plugin\search_api\datasource\ContentEntityTrackingManager;

/**
 * Gets and operates over the related nodes.
 */
class RelatedNodes {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The search_api.entity_datasource.tracking_manager service.
   *
   * @var \Drupal\search_api\Plugin\search_api\datasource\ContentEntityTrackingManager
   */
  protected $searchApiEntityDatasourceTrackingManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a RelatedNodes object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\search_api\Plugin\search_api\datasource\ContentEntityTrackingManager $search_api_entity_datasource_tracking_manager
   *   The search_api.entity_datasource.tracking_manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ContentEntityTrackingManager $search_api_entity_datasource_tracking_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->searchApiEntityDatasourceTrackingManager = $search_api_entity_datasource_tracking_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get related nodes for given node.
   */
  public function getRelatedNodes($node) {
    $type = $node->getType();
    $config = $this->configFactory->get('search_api_reindex_related.settings');
    $related_ids = $config->get($type);
    if (!$related_ids) {
      return [];
    }
    $related_ids = explode(',', $related_ids);
    // Trim in case it's needed.
    foreach ($related_ids as $key => $id) {
      $related_ids[$key] = trim($id);
    }
    return $this->entityTypeManager->getStorage('node')->loadMultiple($related_ids);
  }

  /**
   * Request reindexing for related nodes.
   */
  public function requestReindexingRelatedNodes($node) {
    $related = $this->getRelatedNodes($node);
    foreach ($related as $related_node) {
      // After this, at least a new batch should be indexed to get the pages up-to-date.
      $this->searchApiEntityDatasourceTrackingManager->entityUpdate($related_node);
    }
  }

  /**
   * Request reindexing for related nodes based on given nids.
   */
  public function requestReindexingRelatedNodesFromNids($nids) {
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    foreach ($nodes as $node) {
      $this->requestReindexingRelatedNodes($node);
    }
  }

}
