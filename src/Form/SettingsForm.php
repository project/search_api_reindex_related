<?php

namespace Drupal\search_api_reindex_related\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Search API Reindex Related settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_api_reindex_related_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['search_api_reindex_related.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('node_type');
    $bundles = $storage->loadMultiple();

    foreach ($bundles as $bundle) {
      $form[$bundle->id()] = [
        '#type' => 'textfield',
        '#title' => $this->t('Related node ids for @bundle', ['@bundle' => $bundle->label()]),
        '#default_value' => $this->config('search_api_reindex_related.settings')->get($bundle->id()),
        '#description' => $this->t('Enter a comma separated list of node ids to be used as related nodes for @bundle', ['@bundle' => $bundle->label()]),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('node_type');
    $bundles = $storage->loadMultiple();
    foreach ($bundles as $bundle) {
      $field_id = $bundle->id();
      $value = $form_state->getValue($field_id);
      if (!empty($value)) {
        $nids = explode(',', $value);
        foreach ($nids as $nid) {
          if (!is_numeric($nid)) {
            $form_state->setErrorByName($field_id, $this->t('The value for @field_id is not compound by only valid node ids.', ['@field_id' => $field_id]));
          }
        }
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityTypeManager->getStorage('node_type');
    $bundles = $storage->loadMultiple();
    foreach ($bundles as $bundle) {
      $field_id = $bundle->id();
      $this->config('search_api_reindex_related.settings')
        ->set($field_id, $form_state->getValue($field_id))
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
