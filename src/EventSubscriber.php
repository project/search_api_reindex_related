<?php

namespace Drupal\search_api_reindex_related;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\search_api\Event\SearchApiEvents;
use Drupal\search_api\Event\ItemsIndexedEvent;

/**
 * Provides a service for managing pending tracking tasks for datasources.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * Related nodes services.
   *
   * @var \Drupal\search_api_reindex_related\RelatedNodes
   */
  protected $relatedNodes;

  /**
   * Construct.
   */
  public function __construct(RelatedNodes $related_nodes) {
    $this->relatedNodes = $related_nodes;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[SearchApiEvents::ITEMS_INDEXED][] = ['processItemsIndexed'];
    return $events;
  }

  /**
   * Process items indexed.
   *
   * @param \Drupal\search_api\Event\ItemsIndexedEvent $event
   *   The event.
   */
  public function processItemsIndexed(ItemsIndexedEvent $event) {
    $items = $event->getProcessedIds();
    $nids = [];
    foreach ($items as $item) {
      // Original format is: "entity:node/1:en".
      if (strpos($item, 'entity:node/') === 0) {
        $item = str_replace('entity:node/', '', $item);
        // Remove langcode.
        $item_parts = explode(':', $item);
        $nid = $item_parts[0];
        if (is_numeric($nid)) {
          $nids[] = $nid;
        }
      }
    }
    $this->relatedNodes->requestReindexingRelatedNodesFromNids($nids);
  }

}
